import java.io.{FileReader, LineNumberReader, PrintWriter}

import scala.io.Source
import collection.mutable.{HashMap, HashSet}
import SimilarityApp._

class SimilarityApp {

  var artistSimilarityReport: PrintWriter = _
  var userSimilarityReport: PrintWriter = _
  val userPrefs = HashMap.empty[Int, HashMap[String, Int]]
  val artistFans = HashMap.empty[String, HashSet[Int]]

  def start(filename: String) {
    val data = readData(filename)

    processData(data, linesCount(filename))
    try {
      artistSimilarityReport = new PrintWriter("artist-similarities.tsv")
      calcArtistSimilarities()
    } finally {
      artistSimilarityReport.close()
    }

    try {
      userSimilarityReport = new PrintWriter("user-similarities.tsv")
      calcUserSimilarities()
    } finally {
      userSimilarityReport.close()
    }
  }

  def readData(filepath: String): Iterator[(Int, String)] = {
    Source.fromFile(filepath).getLines().map {
      line =>
        val columns = line.split('\t')
        // userid \t timestamp \t musicbrainz-artist-id \t artist-name \t musicbrainz-track-id \t track-name
        (columns(0).substring(5).toInt, columns(3))
    }.filter {
      case (userId, artistName) =>
        artistName.nonEmpty
    }
  }

  def processData(data: Iterator[(Int, String)], lines: Int): Unit = {
    println("Process data file...")

    printProgress(data, "file", size0 = Some(lines)) {
      case (userId, artist) =>
        val prefs = userPrefs.getOrElseUpdate(userId, HashMap.empty)
        prefs.put(artist, prefs.getOrElse(artist, 0) + 1)
        artistFans.getOrElseUpdate(artist, HashSet.empty) += userId
    }
  }

  def calcArtistSimilarities(): Unit = {
    println("Calculate artists similarity...")

    artistSimilarityReport.write("Artist\tSimilar artist\n")

    printProgress(artistFans, "artists similarity") {
      case (artist, userIds) =>
        val otherArtistScores = userIds.flatMap { userId => userPrefs(userId).toList }.
          foldLeft(Map.empty[String, Int]){
            case (artistScores, (otherArtist, count)) =>
              if (otherArtist == artist)
                artistScores
              else
                artistScores.updated(otherArtist, artistScores.getOrElse(otherArtist, 0) + count)
          }
        if (otherArtistScores.nonEmpty) {
          val (similarArtist, _) = otherArtistScores.maxBy { case (_, score) => score }
          artistSimilarityReport.write(artist); artistSimilarityReport.write("\t")
          artistSimilarityReport.write(similarArtist); artistSimilarityReport.write("\n")
        } else {
          println(s"Artist without similarities: $artist")
        }
    }
  }

  def calcUserSimilarities(): Unit = {
    println("Calculate users similarity...")

    userSimilarityReport.write("User\tSimilar user\n")

    val userVectorSizes = userPrefs.map {
      case (userId, prefs) =>
        userId -> Math.sqrt(prefs.values.map(i => i * i).sum)
    }

    printProgress(userPrefs, "users similarity") {
      case (userId, prefs) =>
        val userVectorSize = userVectorSizes(userId)
        val (similarUserId, prefs2) = userPrefs.filter(_._1 != userId).maxBy {
          case (otherUserId, otherPrefs) =>
            dotProduct(prefs, otherPrefs) / (userVectorSize * userVectorSizes(otherUserId))
        }

        userSimilarityReport.write(formattedUserName(userId)); userSimilarityReport.write("\t")
        userSimilarityReport.write(formattedUserName(similarUserId)); userSimilarityReport.write("\n")
    }
  }

  def dotProduct(prefs1: collection.mutable.Map[String, Int],
                 prefs2: collection.mutable.Map[String, Int]): Double =
    prefs1.map {
      case (artist, count) =>
        prefs2.getOrElse(artist, 0) * count
    }.sum

  def formattedUserName(userId: Int): String = "user_" + "%06d".format(userId)
}

object SimilarityApp {
  def main(args: Array[String]): Unit = {
    var filename = "lastfm-dataset-1K/userid-timestamp-artid-artname-traid-traname.tsv"
    if (args.size > 0 && args.head.nonEmpty)
      filename = args.head
    (new SimilarityApp).start(filename)
  }

  def printProgress[A](traversable: TraversableOnce[A], name: String, size0: Option[Int] = None)(code: (A) => Unit): Unit = {
    val size = size0.getOrElse(traversable.size)
    var i = 0
    val peresentQuantity = math.max(1, (size / 100.0).toInt)
    traversable.foreach {
      item =>
        code(item)
        i += 1
        if (i % peresentQuantity == 0)
          println(s"$name progress: ${(i * 100.0 / size).toInt}%")
    }
  }

  def linesCount(filename: String): Int = {
    val reader = new LineNumberReader(new FileReader(filename))
    reader.skip(Long.MaxValue)
    val value = reader.getLineNumber + 1
    reader.close()
    value
  }
}
